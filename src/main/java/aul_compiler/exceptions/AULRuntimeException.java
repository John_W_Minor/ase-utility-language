package aul_compiler.exceptions;

import aul_compiler.AULLogging;
import org.apache.logging.log4j.Logger;

/**
 * Created by Queue on 3/22/2018.
 */
public class AULRuntimeException extends RuntimeException
{

    public AULRuntimeException(String _message)
    {
        super(_message);
    }

    private static String generateMessage(String _message, int _version)
    {
        return "";
    }

    @Override
    public void printStackTrace()
    {
        Logger logger = AULLogging.getLogger();
        if (logger != null)
        {
            logger.error("", this);
        }
    }

}
