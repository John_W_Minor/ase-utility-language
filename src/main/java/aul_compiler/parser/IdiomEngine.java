package aul_compiler.parser;

import aul_compiler.AULCompiler;
import aul_compiler.AULCompilerLog;
import aul_compiler.exceptions.AULRuntimeException;
import aul_compiler.parser.idiom_data.MethodSignature;
import aul_compiler.parser.idioms.reserved_idioms.boolean_statement_evaluation.integer.IntegerEquals;
import aul_compiler.parser.idioms.reserved_idioms.boolean_statement_evaluation.integer.IntegerGreaterThan;
import aul_compiler.parser.idioms.reserved_idioms.boolean_statement_evaluation.integer.IntegerLessThan;
import aul_compiler.parser.idioms.reserved_idioms.boolean_statement_evaluation.integer.IntegerNotEquals;
import aul_compiler.parser.idioms.reserved_idioms.boolean_statement_evaluation.string.StringEquals;
import aul_compiler.parser.idioms.reserved_idioms.boolean_statement_evaluation.string.StringNotEquals;

import java.util.ArrayList;

/**
 * Created by Queue on 3/21/2018.
 */
public class IdiomEngine
{
    public static final String METHOD_SIGNATURE_SPLIT = "into";
    public static final String IF_SIGNATURE_SPLIT = "else go to";
    public static final String ARGUMENT_SPLIT = ",";

    public static ArrayList<String> noArgMemoryOp(String _line, String _byteCode, String _identifier, int _numericalSubidentifierMinValue, int _numericalSubidentifierMaxValue)
    {
        if (_line == null || _byteCode == null || _identifier == null)
        {
            throw new AULRuntimeException("Null arguments in noArgMemoryOp()");
        }

        Integer subidentifier = ParserEngine.findIdiomSubidentifier(_line, _identifier, _numericalSubidentifierMinValue, _numericalSubidentifierMaxValue);

        if (subidentifier == null)
        {
            return null;
        }

        ArrayList<String> compiledIdiom = new ArrayList<>();

        compiledIdiom.add("pi" + subidentifier);
        compiledIdiom.add(_byteCode);

        return compiledIdiom;
    }

    public static ArrayList<String> methodWithArgs(String _line, AULCompiler _compiler, String[] _byteCodes, String _identifier, MethodSignature[] _loadSignatures, MethodSignature _saveSignature, String _errorLine, AULCompilerLog _compilerLog)
    {
        if (_line == null || _compiler == null || _byteCodes == null || _identifier == null || _loadSignatures == null || _saveSignature == null)
        {
            throw new AULRuntimeException("Null arguments in methodWithArgs()");
        }

        String[] loadSaveArguments = ParserEngine.returnLineSplitInTwoWithoutPrefix(_line, _identifier, METHOD_SIGNATURE_SPLIT);

        if (loadSaveArguments == null || loadSaveArguments.length != 2 || loadSaveArguments[0] == null || loadSaveArguments[1] == null)
        {
            //if we pass this check we know it's using whatever identifier has been passed into this method
            return null;
        }

        String unsplitLoadArguments = ParserEngine.returnLineWithoutOpeningAndClosingParens(loadSaveArguments[0]);
        String unsplitSaveArguments = ParserEngine.returnLineWithoutOpeningAndClosingParens(loadSaveArguments[1]);

        //region load signature
        if (unsplitLoadArguments == null || ParserEngine.doesLineContainOneOrMoreStrings(unsplitLoadArguments, new String[]{"(", ")"}) ||
                unsplitSaveArguments == null || ParserEngine.doesLineContainOneOrMoreStrings(unsplitSaveArguments, new String[]{"(", ")"}))
        {
            //malformed load arguments
            return null;
        }

        String[] splitLoadArguments = unsplitLoadArguments.split(ARGUMENT_SPLIT);

        ArrayList<String> usedLoadSigs = null;

        for (MethodSignature sig : _loadSignatures)
        {
            ArrayList<String> tempLoadSigArgs = ParserEngine.parseMethodSignature(sig);

            if (splitLoadArguments.length == tempLoadSigArgs.size())
            {
                usedLoadSigs = tempLoadSigArgs;
                break;
            }
        }

        if (usedLoadSigs == null)
        {
            return null;
        }

        ArrayList<String> loadArguments = new ArrayList<>();

        for (int i = 0; i < splitLoadArguments.length; i++)
        {
            String argument = splitLoadArguments[i].trim();

            if (argument.contains(ParserEngine.TYPE_MARKER))
            {
                String typing = ParserEngine.TYPE_MARKER + usedLoadSigs.get(i);

                if (argument.contains(typing))
                {
                    argument = argument.substring(typing.length()).trim();
                } else
                {
                    return null;
                }
            } else
            {
                return null;
            }

            loadArguments.add(argument);
        }

        String[] splitSaveArguments = unsplitSaveArguments.split(ARGUMENT_SPLIT);

        ArrayList<String> saveArguments = new ArrayList<>();

        ArrayList<String> usedSaveSigArgs = ParserEngine.parseMethodSignature(_saveSignature);

        if (splitSaveArguments.length != usedSaveSigArgs.size())
        {
            return null;
        }

        for (int i = 0; i < splitSaveArguments.length; i++)
        {
            String argument = splitSaveArguments[i].trim();

            if (argument.contains(ParserEngine.TYPE_MARKER))
            {
                String typing = ParserEngine.TYPE_MARKER + usedSaveSigArgs.get(i);

                if (argument.contains(typing))
                {
                    argument = argument.substring(typing.length()).trim();
                } else
                {
                    return null;
                }
            } else
            {
                return null;
            }

            saveArguments.add(argument);
        }

        ArrayList<String> compiledIdiom = _compiler.partialCompile(loadArguments, true, _compilerLog);

        for (String code : _byteCodes)
        {
            compiledIdiom.add(code);
        }

        ArrayList<String> compiledSaveIdioms = _compiler.partialCompile(saveArguments, false, _compilerLog);

        compiledIdiom.addAll(compiledSaveIdioms);

        return compiledIdiom;
    }

    public static final MethodSignature ELSE_GO_TO_LABEL_SIGNATURE = new MethodSignature(ParserEngine.LABEL_TYPE);

    public static ArrayList<String> ifStatement(String _line, AULCompiler _compiler, String _byteCode, String _identifier, String _errorLine, AULCompilerLog _compilerLog)
    {
        if (_line == null || _compiler == null || _byteCode == null || _identifier == null)
        {
            throw new AULRuntimeException("Null arguments in ifStatement()");
        }

        String[] ifElseArguments = ParserEngine.returnLineSplitInTwoWithoutPrefix(_line, _identifier, IF_SIGNATURE_SPLIT);

        if (ifElseArguments == null || ifElseArguments.length != 2 || ifElseArguments[0] == null || ifElseArguments[1] == null)
        {
            //if we pass this check we know it's using whatever identifier has been passed into this method
            return null;
        }

        String IfArgument = ParserEngine.returnLineWithoutOpeningAndClosingParens(ifElseArguments[0]);
        String ElseArgument = ParserEngine.returnLineWithoutOpeningAndClosingParens(ifElseArguments[1]);

        //region load signature
        if (IfArgument == null || ParserEngine.doesLineContainOneOrMoreStrings(IfArgument, new String[]{"(", ")"}) ||
                ElseArgument == null || ParserEngine.doesLineContainOneOrMoreStrings(ElseArgument, new String[]{"(", ")"}))
        {
            //malformed load arguments
            return null;
        }


        String[] splitElseArguments = ElseArgument.split(ARGUMENT_SPLIT);

        ArrayList<String> labelArgument = new ArrayList<>();

        ArrayList<String> usedElseSigArgs = ParserEngine.parseMethodSignature(ELSE_GO_TO_LABEL_SIGNATURE);

        if (splitElseArguments.length != usedElseSigArgs.size())
        {
            return null;
        }

        for (int i = 0; i < splitElseArguments.length; i++)
        {
            String argument = splitElseArguments[i].trim();

            if (argument.contains(ParserEngine.TYPE_MARKER))
            {
                String typing = ParserEngine.TYPE_MARKER + usedElseSigArgs.get(i);

                if (argument.contains(typing))
                {
                    argument = argument.substring(typing.length()).trim();
                } else
                {
                    return null;
                }
            } else
            {
                return null;
            }

            labelArgument.add(argument);
        }

        ArrayList<String> booleanEvaluation = null;

        if(IntegerEquals.compileToIdiom(IfArgument, _compiler, _compilerLog) != null)
        {
            booleanEvaluation = IntegerEquals.compileToIdiom(IfArgument, _compiler, _compilerLog);
        } else if(IntegerNotEquals.compileToIdiom(IfArgument, _compiler, _compilerLog) != null)
        {
            booleanEvaluation = IntegerNotEquals.compileToIdiom(IfArgument, _compiler, _compilerLog);
        } else if(IntegerGreaterThan.compileToIdiom(IfArgument, _compiler, _compilerLog) != null)
        {
            booleanEvaluation = IntegerGreaterThan.compileToIdiom(IfArgument, _compiler, _compilerLog);
        } else if(IntegerLessThan.compileToIdiom(IfArgument, _compiler, _compilerLog) != null)
        {
            booleanEvaluation = IntegerLessThan.compileToIdiom(IfArgument, _compiler, _compilerLog);
        } else if(StringEquals.compileToIdiom(IfArgument, _compiler, _compilerLog) != null)
        {
            booleanEvaluation = StringEquals.compileToIdiom(IfArgument, _compiler, _compilerLog);
        } else if(StringNotEquals.compileToIdiom(IfArgument, _compiler, _compilerLog) != null)
        {
            booleanEvaluation = StringNotEquals.compileToIdiom(IfArgument, _compiler, _compilerLog);
        }

        if(booleanEvaluation == null)
        {
            return null;
        }

        ArrayList<String> compiledElseIdiom = _compiler.partialCompile(labelArgument, true, _compilerLog);

        ArrayList<String> compiledIdiom = new ArrayList<>();
        compiledIdiom.addAll(booleanEvaluation);
        compiledIdiom.addAll(compiledElseIdiom);
        compiledIdiom.add(_byteCode);

        return compiledIdiom;
    }
}
