package aul_compiler.parser;

import aul_compiler.exceptions.AULRuntimeException;
import aul_compiler.parser.idiom_data.Alias;
import aul_compiler.parser.idiom_data.MethodSignature;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Queue on 3/22/2018.
 */
public class ParserEngine
{
    public static String RESERVED_CHARACTERS = " ,()=@";
    public static String NUMBER_MARKER = "@";
    public static String TYPE_MARKER = "?";
    public static String INTEGER_TYPE = "int";
    public static String STRING_TYPE = "string";
    public static String BYTE_ARRAY_TYPE = "array";
    public static String LABEL_TYPE = "label";
    public static String LINE_SEPARATOR = "\n";

    public static String BOOLEAN_EQUALS = "==";
    public static String BOOLEAN_NOT_EQUALS = "!=";

    public static String BOOLEAN_GREATER_THAN = ">=";
    public static String BOOLEAN_LESS_THAN = "<=";

    public static String ASSIGNMENT_MARKER = "=";

    //region general line analysis
    public static ArrayList<String> separateLinesAndAddLineNumbers(String _multiLine)
    {
        String[] lines = _multiLine.split(LINE_SEPARATOR);

        ArrayList<String> linesList = new ArrayList<>();

        for(int i = 0; i < lines.length; i++)
        {
            String currentLine = i+"@ "+lines[i];
            linesList.add(currentLine);
        }

        return linesList;
    }

    public static String returnLineWithoutLineNumber(String _line)
    {
        if(_line == null)
        {
            throw new AULRuntimeException("Null _line argument in returnLineWithoutLineNumber().");
        }

        String[] splitOnNumber = returnLineSplitInTwo(_line, NUMBER_MARKER);

        if(splitOnNumber == null || splitOnNumber[1] == null)
        {
            return null;
        }

        return splitOnNumber[1];
    }

    public static String[] returnLineSplitInTwoWithoutPrefix(String _line, String _prefix, String _splitter)
    {
        if(_line == null || _prefix == null || _splitter == null)
        {
            throw new AULRuntimeException("Null arguments in returnLineSplitInTwoWithoutPrefix().");
        }

        String sansPrefix = returnLineWithoutPrefix(_line, _prefix);

        if(sansPrefix == null)
        {
            return null;
        }

        return returnLineSplitInTwo(sansPrefix, _splitter);
    }

    public static String returnLineWithoutPrefix(String _line, String _prefix)
    {
        if (_line == null || _prefix == null)
        {
            throw new AULRuntimeException("Null arguments in returnLineWithoutPrefix().");
        }

        try
        {
            _line = _line.trim();
            String possiblePrefix = _line.substring(0, _prefix.length());

            if (possiblePrefix.equals(_prefix))
            {
                return _line.substring(_prefix.length()).trim();
            }

        } catch (Exception e)
        {
            return null;
        }

        return null;
    }

    public static String[] returnLineSplitInTwo(String _line, String _splitter)
    {
        if (_line == null || _splitter == null)
        {
            throw new AULRuntimeException("Null arguments in returnLineSplitInTwo().");
        }

        _line = _line.trim();
        String[] splitLine = _line.split(_splitter);

        if (splitLine.length != 2)
        {
            return null;
        }

        if (splitLine[0] == null || splitLine[1] == null)
        {
            return null;
        }

        splitLine[0] = splitLine[0].trim();
        splitLine[1] = splitLine[1].trim();

        return splitLine;
    }

    public static String returnLineWithoutOpeningAndClosingParens(String _line)
    {
        if (_line == null)
        {
            throw new AULRuntimeException("Null argument in returnLineWithoutOpeningAndClosingParens().");
        }

        _line = _line.trim();
        String openingParen = _line.substring(0, 1);
        String closingParen = _line.substring(_line.length() - 1, _line.length());

        if (openingParen.equals("(") && closingParen.equals(")"))
        {
            return _line.substring(1, _line.length() - 1).trim();
        }

        return null;
    }

    public static String returnLineWithoutOpeningAndClosingQuotes(String _line)
    {
        if (_line == null)
        {
            throw new AULRuntimeException("Null argument in returnLineWithoutOpeningAndClosingParens().");
        }

        _line = _line.trim();
        String openingQuote = _line.substring(0, 1);
        String closingQuote = _line.substring(_line.length() - 1, _line.length());

        if (openingQuote.equals("\"") && closingQuote.equals("\""))
        {
            return _line.substring(1, _line.length() - 1).trim();
        }

        return null;
    }

    public static boolean doesLineContainPrefix(String _line, String _prefix)
    {
        if (_line == null || _prefix == null)
        {
            throw new AULRuntimeException("Null arguments in doesLineContainPrefix().");
        }

        try
        {
            _line = _line.trim();
            String possiblePrefix = _line.substring(0, _prefix.length());

            return possiblePrefix.equals(_prefix);
        } catch (Exception e)
        {
            return false;
        }
    }

    public static boolean doesLineContainOneOrMoreStrings(String _line, String[] _doesContain)
    {
        if (_line == null || _doesContain == null)
        {
            throw new AULRuntimeException("Null arguments in doesLineContainOneOrMoreStrings().");
        }

        for (String doesContain : _doesContain)
        {
            if (_line.contains(doesContain))
            {
                return true;
            }
        }

        return false;
    }

    public static boolean doesLineContainAllStrings(String _line, String[] _doesContain)
    {
        if (_line == null || _doesContain == null)
        {
            throw new AULRuntimeException("Null arguments in doesLineContainAllStrings().");
        }

        for (String doesContain : _doesContain)
        {
            if (!_line.contains(doesContain))
            {
                return false;
            }
        }

        return true;
    }
    //endregion

    //region general line manipulation
    public static String dealiasLine(String _line, Alias _alias)
    {
        if (_line == null || _alias == null)
        {
            throw new AULRuntimeException("Null arguments in dealiasLine().");
        }

        String initLine = _line;

        _line = (" " + _line.trim() + " ").toLowerCase();

        String aliasName = _alias.getAlias().toLowerCase();
        String aliasTarget = _alias.getAliasTarget().toLowerCase();

        aliasName = aliasName.replace("[","\\[");
        aliasName = aliasName.replace("]","\\]");

        Pattern pattern = Pattern.compile("[" + RESERVED_CHARACTERS + "]" + aliasName + "[" + RESERVED_CHARACTERS + "]");
        Matcher matcher = pattern.matcher(_line);

        ArrayList<String> usedTempAliases = new ArrayList<>();
        ArrayList<String> usedTempAliasTargets = new ArrayList<>();

        while (matcher.find())
        {
            String tempAlias = matcher.group();

            boolean success = true;
            for (String used : usedTempAliases)
            {
                if (used.equals(tempAlias))
                {
                    success = false;
                }
            }

            if (success)
            {
                usedTempAliases.add(tempAlias);

                String tempAliasTarget = tempAlias.substring(0, 1) + aliasTarget + tempAlias.substring(tempAlias.length() - 1, tempAlias.length());
                usedTempAliasTargets.add(tempAliasTarget);
            }
            try
            {
                Field field = matcher.getClass().getDeclaredField("last");
                field.setAccessible(true);
                int last = field.getInt(matcher) - 1;
                field.setInt(matcher, last);
            } catch (Exception e)
            {
                return initLine;
            }
        }

        int position = 0;
        for (int i = 0; i < _line.length(); i++)
        {
            for (int k = 0; k < usedTempAliases.size(); k++)
            {
                String tempAlias = usedTempAliases.get(k);
                String tempAliasTarget = usedTempAliasTargets.get(k);

                if (_line.regionMatches(i, tempAlias, 0, tempAlias.length()))
                {
                    String lineFirst = _line.substring(0, i);
                    String lineSecond = _line.substring(i + tempAlias.length(), _line.length());

                    _line = lineFirst + tempAliasTarget + lineSecond;
                    i += tempAliasTarget.length() - 1;
                    i--;
                    break;
                }
            }
        }

        return _line.trim();
    }
    //endregion

    //region idiom analysis
    public static Integer findIdiomSubidentifier(String _line, String _identifier, int _numericalSubidentifierMinValue, int _numericalSubidentifierMaxValue)
    {
        if (_line == null || _identifier == null)
        {
            throw new AULRuntimeException("Null arguments in findIdiomSubidentifierFragment()");
        }

        try
        {
            if (!doesLineContainPrefix(_line, _identifier))
            {
                return null;
            }

            String subidentifier = _line.substring(_identifier.length());

            int subidentifierValue = 0;

            try
            {
                subidentifierValue = Integer.valueOf(subidentifier);
            } catch (Exception e)
            {
                return null;
            }

            boolean properValue = subidentifierValue >= _numericalSubidentifierMinValue && subidentifierValue <= _numericalSubidentifierMaxValue;

            if (!properValue)
            {
                return null;
            }

            return subidentifierValue;
        } catch (Exception e)
        {
            return null;
        }
    }
    //endregion

    //region method signature analysis
    public static ArrayList<String> parseMethodSignature(String _signature)
    {
        if (_signature == null)
        {
            throw new AULRuntimeException("Null _signature argument in parseMethodSignature()");
        }

        _signature = _signature.trim();

        String[] argumentsArray = _signature.split(",");

        ArrayList<String> arguments = new ArrayList<>();

        for (String argument : argumentsArray)
        {
            argument = argument.trim();

            if (argument.equals(INTEGER_TYPE) || argument.equals(STRING_TYPE) || argument.equals(BYTE_ARRAY_TYPE) || argument.equals(LABEL_TYPE))
            {
                arguments.add(argument);
            } else
            {
                throw new AULRuntimeException("Bad argument type in signature in parseMethodSignature()");
            }
        }

        return arguments;
    }

    public static ArrayList<String> parseMethodSignature(MethodSignature _signature)
    {
        if (_signature == null)
        {
            throw new AULRuntimeException("Null _signature argument in parseMethodSignature()");
        }

        return parseMethodSignature(_signature.getSignature());
    }
    //endregion

    //region Array Analysis
    public static String[] returnWordAndArrayRange(String _wordWithRange)
    {
        String[] wordAndRange = ParserEngine.returnLineSplitInTwo(_wordWithRange, "\\[");

        if (wordAndRange == null)
        {
            return null;
        }

        if (wordAndRange[1].contains("]"))
        {
            wordAndRange[1] = wordAndRange[1].replace("]", " ").trim();
        } else
        {
            return null;
        }

        return wordAndRange;
    }

    public static int[] returnArrayRange(String _range)
    {
        String[] stringRange = ParserEngine.returnLineSplitInTwo(_range, "~");

        if (stringRange == null)
        {
            return null;
        }

        int[] range = new int[2];

        try
        {
            range[0] = Integer.parseInt(stringRange[0]);
            range[1] = Integer.parseInt(stringRange[1])+1;
        }catch (Exception e)
        {
            return null;
        }

        return range;
    }
    //endregion
}
