package aul_compiler.parser.idioms;

import aul_compiler.AULCompiler;
import aul_compiler.AULCompilerLog;

import java.util.ArrayList;

/**
 * Created by Queue on 3/21/2018.
 */
public interface IIdiom
{
    ArrayList<String> compileToIdiom(String _line, AULCompiler _compiler, boolean _loadMode, AULCompilerLog _compilerLog);
}
