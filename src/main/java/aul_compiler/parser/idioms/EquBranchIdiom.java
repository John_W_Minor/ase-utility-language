package aul_compiler.parser.idioms;

import aul_compiler.AULCompiler;
import aul_compiler.AULCompilerLog;
import aul_compiler.parser.ParserEngine;
import aul_compiler.parser.IdiomEngine;
import aul_compiler.parser.idiom_data.MethodSignature;

import java.util.ArrayList;

/**
 * Created by Queue on 3/21/2018.
 */
public class EquBranchIdiom implements IIdiom
{
    public static final String[] BYTECODE = {"noop", "equbrnch"};
    public static final String IDENTIFIER = "equbranch";
    public static final MethodSignature[] LOAD_SIGNATURE = {new MethodSignature(ParserEngine.LABEL_TYPE),
            new MethodSignature(ParserEngine.INTEGER_TYPE + "," + ParserEngine.INTEGER_TYPE + "," + ParserEngine.LABEL_TYPE)};
    public static final MethodSignature SAVE_SIGNATURE = new MethodSignature(ParserEngine.STRING_TYPE + "," + ParserEngine.INTEGER_TYPE);

    public ArrayList<String> compileToIdiom(String _line, AULCompiler _compiler, boolean _loadMode, AULCompilerLog _compilerLog)
    {
        if (_line == null || _compiler == null)
        {
            return null;
        }

        if (!_loadMode)
        {
            return null;
        }

        String lineSansNumber = ParserEngine.returnLineWithoutLineNumber(_line);
        String errorLine = _line;

        if(lineSansNumber == null)
        {
            return null;
        }

        return IdiomEngine.methodWithArgs(lineSansNumber, _compiler, BYTECODE, IDENTIFIER, LOAD_SIGNATURE, SAVE_SIGNATURE, errorLine, _compilerLog);
    }
}
