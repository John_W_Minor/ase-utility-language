package aul_compiler.parser.idioms.reserved_idioms.memory;

import aul_compiler.AULCompiler;
import aul_compiler.AULCompilerLog;
import aul_compiler.AULLogging;
import aul_compiler.exceptions.AULRuntimeException;
import aul_compiler.parser.ParserEngine;
import aul_compiler.parser.IdiomEngine;
import aul_compiler.parser.idiom_data.ConstantMemoryAssignment;
import aul_compiler.parser.idiom_data.DataTyping;
import aul_compiler.parser.idioms.IIdiom;
import engine.ASEConstants;
import engine.data.ConstantMemory;
import engine.data.DataElement;
import engine.operators.reserved_set.word8v1.enumeration.Word8v1Enum;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Queue on 3/21/2018.
 */
public class ConstIdiom implements IIdiom
{
    public static final String BYTECODE = Word8v1Enum.LOAD_CONSTANT_TO_STACK.getKeyword();
    public static final String IDENTIFIER = "const";
    public static final int NUMERICAL_SUBIDENTIFIER_MIN_VALUE = 0;
    public static final int NUMERICAL_SUBIDENTIFIER_MAX_VALUE = 31;

    public ArrayList<String> compileToIdiom(String _line, AULCompiler _compiler, boolean _loadMode, AULCompilerLog _compilerLog)
    {
        if (_line == null || _compiler == null)
        {
            return null;
        }

        if (!_loadMode)
        {
            return null;
        }

        String withoutLineNumber = ParserEngine.returnLineWithoutLineNumber(_line);

        if (withoutLineNumber != null)
        {
            _line = withoutLineNumber;
        }

        String lineSansInt = ParserEngine.returnLineWithoutPrefix(_line, ParserEngine.TYPE_MARKER + ParserEngine.INTEGER_TYPE);
        String lineSansString = ParserEngine.returnLineWithoutPrefix(_line, ParserEngine.TYPE_MARKER + ParserEngine.STRING_TYPE);
        String lineSansArray = ParserEngine.returnLineWithoutPrefix(_line, ParserEngine.TYPE_MARKER + ParserEngine.BYTE_ARRAY_TYPE);

        if (lineSansInt != null)
        {
            _line = lineSansInt;
        } else if (lineSansString != null)
        {
            _line = lineSansString;
        } else if (lineSansArray != null)
        {
            _line = lineSansArray;
        }

        return IdiomEngine.noArgMemoryOp(_line, BYTECODE, IDENTIFIER, NUMERICAL_SUBIDENTIFIER_MIN_VALUE, NUMERICAL_SUBIDENTIFIER_MAX_VALUE);
    }

    public static ArrayList<ConstantMemoryAssignment> getConstantMemoryAssignments(ArrayList<String> _source, boolean _remove, AULCompilerLog _compilerLog)
    {
        if (_source == null)
        {
            throw new AULRuntimeException("Null _source argument in getConstantMemoryAssignments().");
        }

        ArrayList<String> source = null;
        if (_remove)
        {
            source = _source;
        } else
        {
            source = new ArrayList<>();
            source.addAll(_source);
        }

        ArrayList<Integer> usedSubidentifiers = new ArrayList<>();

        ArrayList<ConstantMemoryAssignment> assignments = new ArrayList<>();

        Iterator<String> sourceIterator = source.iterator();
        while (sourceIterator.hasNext())
        {
            String line = sourceIterator.next();

            String errorLine = line;

            line = ParserEngine.returnLineWithoutLineNumber(line);

            if(line == null)
            {
                //i don't know
                continue;
            }

            String lineSansInt = ParserEngine.returnLineWithoutPrefix(line, ParserEngine.INTEGER_TYPE);
            String lineSansString = ParserEngine.returnLineWithoutPrefix(line, ParserEngine.STRING_TYPE);
            String lineSansArray = ParserEngine.returnLineWithoutPrefix(line, ParserEngine.BYTE_ARRAY_TYPE);

            String type = null;

            String lineSansPrefix = null;

            if (lineSansInt != null)
            {
                type = ParserEngine.INTEGER_TYPE;
                lineSansPrefix = lineSansInt;
            } else if (lineSansString != null)
            {
                type = ParserEngine.STRING_TYPE;
                lineSansPrefix = lineSansString;
            } else if (lineSansArray != null)
            {
                type = ParserEngine.BYTE_ARRAY_TYPE;
                lineSansPrefix = lineSansArray;
            } else
            {
                continue;
            }

            String[] splitAssignment = ParserEngine.returnLineSplitInTwo(lineSansPrefix, ParserEngine.ASSIGNMENT_MARKER);

            if (splitAssignment == null)
            {
                //not everything that mentions types is assignment
                continue;
            }

            String idiom = splitAssignment[0].trim();
            String value = splitAssignment[1].trim();

            if (type.equals(ParserEngine.STRING_TYPE))
            {
                String tempValue = ParserEngine.returnLineWithoutOpeningAndClosingQuotes(value);

                if (tempValue == null)
                {
                    //if the type is string it needs opening and closing quotes on the data
                    _compilerLog.addCompilerOutput("Non-string value assigned to string type: " + errorLine);
                } else
                {
                    value = tempValue;
                }
            }

            Integer subidentifier = ParserEngine.findIdiomSubidentifier(idiom, IDENTIFIER, NUMERICAL_SUBIDENTIFIER_MIN_VALUE, NUMERICAL_SUBIDENTIFIER_MAX_VALUE);

            if (subidentifier == null)
            {
                //not everything that mentions types is assignment
                continue;
            }

            boolean success = true;
            for (Integer used : usedSubidentifiers)
            {
                if (used.equals(subidentifier))
                {
                    success = false;
                    break;
                }
            }

            if (!success)
            {
                _compilerLog.addCompilerOutput("Constant was assigned more than once: " + errorLine);
            }

            usedSubidentifiers.add(subidentifier);

            assignments.add(new ConstantMemoryAssignment(type, subidentifier, value, errorLine));

            if (_remove)
            {
                sourceIterator.remove();
            }
        }

        for (int i = 0; i < usedSubidentifiers.size(); i++)
        {
            boolean success = false;
            for (Integer used : usedSubidentifiers)
            {
                if (used == i)
                {
                    success = true;
                }
            }
            if (!success)
            {
                _compilerLog.addCompilerOutput("Compilation failed because there was a gap in constant memory addresses used. This limitation exists to allow binaries to be as small as possible.");
            }
        }

        return assignments;
    }

    public static ConstantMemory buildConstantMemory(ArrayList<String> _source, ArrayList<ConstantMemoryAssignment> _assignments, AULCompilerLog _compilerLog)
    {
        if (_source == null || _assignments == null)
        {
            throw new AULRuntimeException("Null arguments in buildConstantMemory().");
        }

        ArrayList<ConstantMemoryAssignment> assignments = new ArrayList<>();
        assignments.addAll(_assignments);

        DataElement[] dataElements = new DataElement[ASEConstants.CONSTANT_MEMORY_MAX_DATA_ELEMENTS];

        for (ConstantMemoryAssignment assignment : assignments)
        {
            String type = assignment.getType();
            Integer subidentifier = assignment.getSubidentifier();
            String value = assignment.getValue();
            byte[] data = null;

            if (type.equals(ParserEngine.INTEGER_TYPE))
            {
                try
                {
                    BigInteger intValue = new BigInteger(value, 10);
                    data = intValue.toByteArray();
                }catch (Exception e)
                {
                    _compilerLog.addCompilerOutput("Non-integer value assigned to integer type: " + assignment.getErrorLine());
                    data = value.getBytes();
                }
            } else if (type.equals(ParserEngine.STRING_TYPE))
            {
                data = value.getBytes();
            } else// if (type.equals(ParserEngine.BYTE_ARRAY_TYPE))
            {
                return null;
            }

            try
            {
                DataElement element = new DataElement(data);

                dataElements[subidentifier] = element;
            } catch (Exception e)
            {
                return null;
            }
        }

        try
        {
            return new ConstantMemory(dataElements);
        } catch (Exception e)
        {
            return null;
        }
    }

    public static ArrayList<DataTyping> getDataTypings(ArrayList<String> _source, boolean _remove, AULCompilerLog _compilerLog)
    {
        if (_source == null)
        {
            throw new AULRuntimeException("Null _source in getDataTypings().");
        }

        ArrayList<ConstantMemoryAssignment> assignments = getConstantMemoryAssignments(_source, _remove, _compilerLog);

        ArrayList<DataTyping> dataTypings = new ArrayList<>();

        for (ConstantMemoryAssignment assignment : assignments)
        {
            String type = assignment.getType();
            String name = IDENTIFIER + assignment.getSubidentifier();

            dataTypings.add(new DataTyping(type, name));
        }

        return dataTypings;
    }
}
