package aul_compiler.parser.idioms.reserved_idioms.memory;

import aul_compiler.AULCompiler;
import aul_compiler.AULCompilerLog;
import aul_compiler.AULLogging;
import aul_compiler.exceptions.AULRuntimeException;
import aul_compiler.parser.IdiomEngine;
import aul_compiler.parser.ParserEngine;
import aul_compiler.parser.idiom_data.DataTyping;
import aul_compiler.parser.idioms.IIdiom;
import engine.operators.reserved_set.word8v1.enumeration.Word8v1Enum;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Queue on 3/21/2018.
 */
public class MemIdiom implements IIdiom
{
    public static final String LOAD_BYTECODE = Word8v1Enum.LOAD_MEMORY_TO_STACK.getKeyword();
    public static final String SAVE_BYTECODE = Word8v1Enum.SAVE_STACK_TO_MEMORY.getKeyword();
    public static final String IDENTIFIER = "memory";
    public static final int NUMERICAL_SUBIDENTIFIER_MIN_VALUE = 0;
    public static final int NUMERICAL_SUBIDENTIFIER_MAX_VALUE = 31;

    public ArrayList<String> compileToIdiom(String _line, AULCompiler _compiler, boolean _loadMode, AULCompilerLog _compilerLog)
    {
        if (_line == null || _compiler == null)
        {
            return null;
        }

        String withoutLineNumber = ParserEngine.returnLineWithoutLineNumber(_line);

        if (withoutLineNumber != null)
        {
            _line = withoutLineNumber;
        }

        String lineSansInt = ParserEngine.returnLineWithoutPrefix(_line, ParserEngine.TYPE_MARKER + ParserEngine.INTEGER_TYPE);
        String lineSansString = ParserEngine.returnLineWithoutPrefix(_line, ParserEngine.TYPE_MARKER + ParserEngine.STRING_TYPE);
        String lineSansArray = ParserEngine.returnLineWithoutPrefix(_line, ParserEngine.TYPE_MARKER + ParserEngine.BYTE_ARRAY_TYPE);

        if (lineSansInt != null)
        {
            _line = lineSansInt;
        } else if (lineSansString != null)
        {
            _line = lineSansString;
        } else if (lineSansArray != null)
        {
            _line = lineSansArray;
        }

        if (_loadMode)
        {
            return IdiomEngine.noArgMemoryOp(_line, LOAD_BYTECODE, IDENTIFIER, NUMERICAL_SUBIDENTIFIER_MIN_VALUE, NUMERICAL_SUBIDENTIFIER_MAX_VALUE);
        } else
        {
            return IdiomEngine.noArgMemoryOp(_line, SAVE_BYTECODE, IDENTIFIER, NUMERICAL_SUBIDENTIFIER_MIN_VALUE, NUMERICAL_SUBIDENTIFIER_MAX_VALUE);
        }
    }

    public static ArrayList<DataTyping> getDataTypings(ArrayList<String> _source, boolean _remove, AULCompilerLog _compilerLog)
    {
        if (_source == null)
        {
            throw new AULRuntimeException("Null _source in getDataTypings().");
        }
        ArrayList<String> source = null;
        if (_remove)
        {
            source = _source;
        } else
        {
            source = new ArrayList<>();
            source.addAll(_source);
        }


        ArrayList<Integer> usedSubidentifiers = new ArrayList<>();

        ArrayList<DataTyping> dataTypings = new ArrayList<>();

        Iterator<String> sourceIterator = source.iterator();
        while (sourceIterator.hasNext())
        {
            String line = sourceIterator.next();

            String errorLine = line;

            line = ParserEngine.returnLineWithoutLineNumber(line);

            if (line == null)
            {
                //i don't know
                continue;
            }

            String lineSansInt = ParserEngine.returnLineWithoutPrefix(line, ParserEngine.INTEGER_TYPE);
            String lineSansString = ParserEngine.returnLineWithoutPrefix(line, ParserEngine.STRING_TYPE);
            String lineSansArray = ParserEngine.returnLineWithoutPrefix(line, ParserEngine.BYTE_ARRAY_TYPE);

            String type = null;

            String lineSansPrefix = null;

            if (lineSansInt != null)
            {
                type = ParserEngine.INTEGER_TYPE;
                lineSansPrefix = lineSansInt;
            } else if (lineSansString != null)
            {
                type = ParserEngine.STRING_TYPE;
                lineSansPrefix = lineSansString;
            } else if (lineSansArray != null)
            {
                type = ParserEngine.BYTE_ARRAY_TYPE;
                lineSansPrefix = lineSansArray;
            } else
            {
                continue;
            }

            Integer subidentifier = ParserEngine.findIdiomSubidentifier(lineSansPrefix, IDENTIFIER, NUMERICAL_SUBIDENTIFIER_MIN_VALUE, NUMERICAL_SUBIDENTIFIER_MAX_VALUE);

            if (subidentifier == null)
            {
                //not everything that mentions type is telling the compiler what kind of data goes in a memory address
                continue;
            }

            boolean success = true;
            for (Integer used : usedSubidentifiers)
            {
                if (used.equals(subidentifier))
                {
                    success = false;
                    break;
                }
            }

            if (!success)
            {
                _compilerLog.addCompilerOutput("Writable memory type was defined more than once: " + errorLine);
            }

            usedSubidentifiers.add(subidentifier);

            dataTypings.add(new DataTyping(type, lineSansPrefix));

            if (_remove)
            {
                sourceIterator.remove();
            }
        }

        return dataTypings;
    }
}
