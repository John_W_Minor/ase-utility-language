package aul_compiler.parser.idioms.reserved_idioms.jump_labels;

import aul_compiler.AULCompiler;
import aul_compiler.AULCompilerLog;
import aul_compiler.AULLogging;
import aul_compiler.exceptions.AULRuntimeException;
import aul_compiler.parser.ParserEngine;
import aul_compiler.parser.idioms.IIdiom;
import engine.ASEConstants;
import engine.data.JumpMemory;

import java.util.ArrayList;

/**
 * Created by Queue on 3/21/2018.
 */
public class PlaceLabelIdiom implements IIdiom
{
    public static final String JUMP_MARKER = "!";
    public static final String IDENTIFIER = "place";

    @Override
    public ArrayList<String> compileToIdiom(String _line, AULCompiler _compiler, boolean _loadMode, AULCompilerLog _compilerLog)
    {
        if (_line == null || _compiler == null)
        {
            return null;
        }

        if (!_loadMode)
        {
            return null;
        }

        String withoutLineNumber = ParserEngine.returnLineWithoutLineNumber(_line);

        if (withoutLineNumber != null)
        {
            _line = withoutLineNumber;
        }

        String label = ParserEngine.returnLineWithoutPrefix(_line, IDENTIFIER);

        if (label == null)
        {
            return null;
        }

        label = ParserEngine.returnLineWithoutPrefix(label, ParserEngine.TYPE_MARKER + ParserEngine.LABEL_TYPE);

        if (label == null)
        {
            return null;
        }

        Integer labelSubidentifier = ParserEngine.findIdiomSubidentifier(label, LabelIdiom.IDENTIFIER, LabelIdiom.NUMERICAL_SUBIDENTIFIER_MIN_VALUE, LabelIdiom.NUMERICAL_SUBIDENTIFIER_MAX_VALUE);

        if (labelSubidentifier == null)
        {
            return null;
        }

        ArrayList<String> compiledIdiom = new ArrayList<>();
        compiledIdiom.add(JUMP_MARKER + LabelIdiom.IDENTIFIER + labelSubidentifier);

        return compiledIdiom;
    }

    public static JumpMemory buildJumpMemory(ArrayList<String> _byteCode, AULCompilerLog _compilerLog)
    {
        if (_byteCode == null)
        {
            throw new AULRuntimeException("Null _byteCode argument in buildJumpMemory()");
        }

        try
        {
            short[] jumpMemoryArray = new short[ASEConstants.JUMP_MEMORY_MAX_ELEMENTS];

            ArrayList<Short> usedJumpPoints = new ArrayList<>();

            for (short i = 0; i < _byteCode.size(); i++)
            {
                String code = _byteCode.get(i);

                String label = ParserEngine.returnLineWithoutPrefix(code, JUMP_MARKER);

                if (label == null)
                {
                    continue;
                }

                Integer labelSubidentifier = ParserEngine.findIdiomSubidentifier(label, LabelIdiom.IDENTIFIER,
                        LabelIdiom.NUMERICAL_SUBIDENTIFIER_MIN_VALUE, LabelIdiom.NUMERICAL_SUBIDENTIFIER_MAX_VALUE);

                if (labelSubidentifier == null)
                {
                    _compilerLog.addCompilerOutput("Unable to process jump point: " + code);
                    continue;
                }

                boolean success = true;
                for (Short used : usedJumpPoints)
                {
                    if (used == i)
                    {
                        success = false;
                        break;
                    }
                }

                if (!success)
                {
                    _compilerLog.addCompilerOutput("Jump point was defined more than once: " + code);
                    continue;
                }

                usedJumpPoints.add(i);

                jumpMemoryArray[labelSubidentifier] = i;

                _byteCode.remove(i);
                i--;
            }

            return new JumpMemory(jumpMemoryArray);
        } catch (Exception e)
        {
            return null;
        }
    }
}
