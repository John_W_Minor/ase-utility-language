package aul_compiler.parser.idioms.reserved_idioms.jump_labels;

import aul_compiler.AULCompiler;
import aul_compiler.AULCompilerLog;
import aul_compiler.parser.IdiomEngine;
import aul_compiler.parser.ParserEngine;
import aul_compiler.parser.idioms.IIdiom;
import engine.operators.reserved_set.word8v1.enumeration.Word8v1Enum;

import java.util.ArrayList;

/**
 * Created by Queue on 3/21/2018.
 */
public class LabelIdiom implements IIdiom
{
    public static final String BYTECODE = Word8v1Enum.LOAD_JUMP_TO_STACK.getKeyword();
    public static final String IDENTIFIER = "label";
    public static final int NUMERICAL_SUBIDENTIFIER_MIN_VALUE = 0;
    public static final int NUMERICAL_SUBIDENTIFIER_MAX_VALUE = 15;

    public ArrayList<String> compileToIdiom(String _line, AULCompiler _compiler, boolean _loadMode, AULCompilerLog _compilerLog)
    {
        if (_line == null || _compiler == null)
        {
            return null;
        }

        if (!_loadMode)
        {
            return null;
        }

        String withoutLineNumber = ParserEngine.returnLineWithoutLineNumber(_line);

        if(withoutLineNumber != null)
        {
            _line = withoutLineNumber;
        }

        String lineSansLabel = ParserEngine.returnLineWithoutPrefix(_line, ParserEngine.TYPE_MARKER + ParserEngine.LABEL_TYPE);

        if(lineSansLabel != null)
        {
            _line = lineSansLabel;
        }

        return IdiomEngine.noArgMemoryOp(_line, BYTECODE, IDENTIFIER, NUMERICAL_SUBIDENTIFIER_MIN_VALUE, NUMERICAL_SUBIDENTIFIER_MAX_VALUE);
    }
}
