package aul_compiler.parser.idioms.reserved_idioms.alias;

import aul_compiler.exceptions.AULRuntimeException;
import aul_compiler.parser.ParserEngine;
import aul_compiler.parser.idiom_data.Alias;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Queue on 3/22/2018.
 */
public class AliasPseudoIdiom
{
    public static final String IDENTIFIER = "def";

    public static final String CONSTANT_IDENTIFIER = "constant";
    public static final String INT_IDENTIFIER = ParserEngine.INTEGER_TYPE;
    public static final String STRING_IDENTIFIER = ParserEngine.STRING_TYPE;
    public static final String ARRAY_IDENTIFIER = ParserEngine.BYTE_ARRAY_TYPE;

    public static ArrayList<Alias> getAliases(ArrayList<String> _source, boolean _remove)
    {
        if (_source == null)
        {
            throw new AULRuntimeException("Null _source argument in getAliases()");
        }

        ArrayList<String> source = null;
        if (_remove)
        {
            source = _source;
        } else
        {
            source = new ArrayList<>();
            source.addAll(_source);
        }

        ArrayList<Alias> aliases = new ArrayList<>();

        Iterator<String> sourceIterator = source.iterator();
        while (sourceIterator.hasNext())
        {
            String line = sourceIterator.next().trim();

            String errorLine = line;

            line = ParserEngine.returnLineWithoutLineNumber(line);

            if (line == null)
            {
                continue;
            }

            String aliasString = ParserEngine.returnLineWithoutPrefix(line, IDENTIFIER);

            if (aliasString != null)
            {
                String[] splitAlias = ParserEngine.returnLineSplitInTwo(aliasString, ParserEngine.ASSIGNMENT_MARKER);

                if (splitAlias == null)
                {
                    continue;
                }

                String aliasTarget = splitAlias[0];

                String alias = splitAlias[1];

                if (!(aliasTarget.contains("[") && alias.contains("[") && aliasTarget.contains("]") && alias.contains("]")))
                {
                    if (_remove)
                    {
                        sourceIterator.remove();
                    }

                    aliases.add(new Alias(alias, aliasTarget));
                    continue;
                }

                String[] arrayAliasWithRange = ParserEngine.returnWordAndArrayRange(alias);

                if (arrayAliasWithRange == null)
                {
                    return null;
                }

                int[] arrayRange = ParserEngine.returnArrayRange(arrayAliasWithRange[1]);

                if (arrayRange == null || arrayRange[0] >= arrayRange[1])
                {
                    return null;
                }

                int low = arrayRange[0];
                int high = arrayRange[1];

                alias = arrayAliasWithRange[0];

                aliasTarget = aliasTarget.replace("[", " ").trim();
                aliasTarget = aliasTarget.replace("]", " ").trim();

                for (; low < high; low++)
                {
                    aliases.add(new Alias(alias + "[" + low + "]", aliasTarget + low));
                }

                if (_remove)
                {
                    sourceIterator.remove();
                }
            }
        }

        return aliases;
    }

    public static ArrayList<String> dealiasSource(ArrayList<String> _source, ArrayList<Alias> _aliases)
    {
        if (_source == null || _aliases == null)
        {
            throw new AULRuntimeException("Null arguments in dealiasSource()");
        }

        ArrayList<String> source = new ArrayList<>();
        source.addAll(_source);

        for (Alias alias : _aliases)
        {
            for (int i = 0; i < source.size(); i++)
            {
                String line = source.get(i);

                line = ParserEngine.dealiasLine(line, alias);

                source.set(i, line);
            }
        }

        return source;
    }
}
