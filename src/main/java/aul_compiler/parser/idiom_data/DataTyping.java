package aul_compiler.parser.idiom_data;

import aul_compiler.exceptions.AULRuntimeException;
import aul_compiler.parser.ParserEngine;

/**
 * Created by Queue on 3/22/2018.
 */
public class DataTyping
{
    String type = null;
    String name = null;

    public DataTyping(String _type, String _name)
    {
        if(_type == null || _name ==null)
        {
            throw new AULRuntimeException("Null arguments in DataTyping().");
        }

        type = ParserEngine.TYPE_MARKER + _type;
        name = _name;
    }

    public String getType()
    {
        return type;
    }

    public String getName()
    {
        return name;
    }

    public Alias getAlias()
    {
        return new Alias(name, type + " " + name);
    }
}
