package aul_compiler.parser.idiom_data;

import aul_compiler.exceptions.AULRuntimeException;

/**
 * Created by Queue on 3/22/2018.
 */
public class ConstantMemoryAssignment
{
    private String type = null;
    private Integer subidentifier = null;
    private String value = null;
    private String errorLine = null;

    public ConstantMemoryAssignment(String _type, Integer _subidentifier, String _value, String _errorLine)
    {
        if (_type == null || _subidentifier == null || _value == null || _errorLine == null)
        {
            throw new AULRuntimeException("Null arguments in ConstantMemoryAssignment().");
        }

        type = _type.trim();
        subidentifier = _subidentifier;
        value = _value.trim();
        errorLine=_errorLine;
    }

    public String getType()
    {
        return type;
    }

    public Integer getSubidentifier()
    {
        return subidentifier;
    }

    public String getValue()
    {
        return value;
    }

    public String getErrorLine()
    {
        return errorLine;
    }
}
