package aul_compiler.parser.idiom_data;

import aul_compiler.exceptions.AULRuntimeException;
import aul_compiler.parser.ParserEngine;

/**
 * Created by Queue on 3/22/2018.
 */
public class MethodSignature
{
    private String signature = null;

    public MethodSignature(String _signature)
    {
        if(_signature == null)
        {
            throw new AULRuntimeException("Null _signature argument in MethodSignature()");
        }

        signature = _signature;

        ParserEngine.parseMethodSignature(signature);
    }

    public String getSignature()
    {
        return signature;
    }
}
