package aul_compiler.parser.idiom_data;

import aul_compiler.exceptions.AULRuntimeException;

/**
 * Created by Queue on 3/22/2018.
 */
public class Alias
{
    private String alias = null;
    private String aliasTarget = null;

    public Alias(String _alias, String _aliasTarget)
    {
        if(_alias == null || _aliasTarget == null)
        {
            throw new AULRuntimeException("Null arguments in Alias().");
        }

        alias=_alias.trim();
        aliasTarget=_aliasTarget.trim();
    }

    public String getAlias()
    {
        return alias;
    }

    public String getAliasTarget()
    {
        return aliasTarget;
    }
}
