package aul_compiler;

import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Queue on 3/29/2018.
 */
public class AULCompilerLog
{
    private ArrayList<String> compilerOutput = new ArrayList<>();

    public void addCompilerOutput(String _output)
    {
        if(_output == null)
        {
            Logger logger = AULLogging.getLogger();
            if (logger != null)
            {
                logger.warn("Null _output passed to addCompilerOutput()");
            }
            return;
        }

        boolean success = true;
        for(String oldOutput: compilerOutput)
        {
            if(_output.equals(oldOutput))
            {
                success = false;
                break;
            }
        }

        if(success)
        {
            compilerOutput.add(_output);
        }
    }

    public void clearCompilerOutput()
    {
        compilerOutput.clear();
    }

    public ArrayList<String> getCompilerOutput()
    {
        ArrayList<String> tempOut = new ArrayList<>();
        tempOut.addAll(compilerOutput);
        return tempOut;
    }
}
