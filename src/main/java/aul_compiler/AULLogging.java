package aul_compiler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Created by Queue on 3/22/2018.
 */
public class AULLogging
{
    private volatile static Logger logger;
    private volatile static boolean isLogging = false;

    public synchronized static void startLogging()
    {
        if (logger == null)
        {
            logger = LogManager.getLogger("AUL Logger");
        }
        isLogging = true;
        logger.info("AUL logging enabled.");
    }

    public synchronized static void stopLogging()
    {
        logger.info("AUL logging disabled.");
        isLogging = false;
    }

    public synchronized static Logger getLogger()
    {
        if(isLogging)
        {
            return logger;
        }
        return null;
    }

    public synchronized boolean isLoggingEnabled()
    {
        return isLogging;
    }
}
