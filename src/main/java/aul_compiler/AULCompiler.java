package aul_compiler;

import aul_compiler.exceptions.AULRuntimeException;
import aul_compiler.parser.ParserEngine;
import aul_compiler.parser.idiom_data.Alias;
import aul_compiler.parser.idiom_data.ConstantMemoryAssignment;
import aul_compiler.parser.idiom_data.DataTyping;
import aul_compiler.parser.idioms.*;
import aul_compiler.parser.idioms.reserved_idioms.alias.AliasPseudoIdiom;
import aul_compiler.parser.idioms.reserved_idioms.boolean_statement_evaluation.IfIdiom;
import aul_compiler.parser.idioms.reserved_idioms.jump_labels.LabelIdiom;
import aul_compiler.parser.idioms.reserved_idioms.jump_labels.PlaceLabelIdiom;
import aul_compiler.parser.idioms.reserved_idioms.memory.ConstIdiom;
import aul_compiler.parser.idioms.reserved_idioms.memory.MemIdiom;
import engine.ASEConstants;
import engine.ByteCodeEngine;
import engine.binary.Binary;
import engine.data.ConstantMemory;
import engine.data.JumpMemory;
import engine.operators.IOperator;
import engine.program.Program;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Queue on 3/21/2018.
 */
public class AULCompiler
{

    public static final String SPLIT = "=";
    public static final int SPLIT_MAX = 2;

    private ArrayList<IIdiom> idioms = new ArrayList<>();
    private int version = 1;

    public AULCompiler()
    {
        //idioms.add(new EquBranchIdiom());
        idioms.add(new IfIdiom());
        idioms.add(new ConstIdiom());
        idioms.add(new LabelIdiom());
        idioms.add(new MemIdiom());
        idioms.add(new PlaceLabelIdiom());
    }

    public ArrayList<String> removeBlankSpace(ArrayList<String> _source)
    {
        if (_source == null)
        {
            throw new AULRuntimeException("Null _source argument in removeBlanks()");
        }

        Logger logger = AULLogging.getLogger();
        if (logger != null)
        {
            logger.debug("Removing blank space.");
        }

        ArrayList<String> source = new ArrayList<>();
        source.addAll(_source);

        Iterator<String> sourceIterator = source.iterator();
        while (sourceIterator.hasNext())
        {
            String line = sourceIterator.next().trim();
            if (ParserEngine.returnLineWithoutLineNumber(line) == null)
            {
                sourceIterator.remove();
            }
        }

        return source;
    }

    public ArrayList<String> unrollLoops(ArrayList<String> _source)
    {
        return null;
    }

    public ArrayList<String> dealiasSource(ArrayList<String> _source)
    {
        if (_source == null)
        {
            throw new AULRuntimeException("Null _source argument in dealiasSource()");
        }

        Logger logger = AULLogging.getLogger();
        if (logger != null)
        {
            logger.debug("Dealiasing Source.");
        }

        ArrayList<String> source = new ArrayList<>();
        source.addAll(_source);

        ArrayList<Alias> aliases = AliasPseudoIdiom.getAliases(source, true);

        return AliasPseudoIdiom.dealiasSource(source, aliases);
    }

    public ConstantMemory buildConstantMemory(ArrayList<String> _source, AULCompilerLog _compilerLog)
    {
        if (_source == null)
        {
            throw new AULRuntimeException("Null _source argument in buildConstantMemory()");
        }

        Logger logger = AULLogging.getLogger();
        if (logger != null)
        {
            logger.debug("Building constant memory.");
        }

        ArrayList<String> source = new ArrayList<>();
        source.addAll(_source);

        ArrayList<ConstantMemoryAssignment> assignments = ConstIdiom.getConstantMemoryAssignments(source, false, _compilerLog);

        return ConstIdiom.buildConstantMemory(source, assignments, _compilerLog);
    }

    public ArrayList<String> insertTypeChecks(ArrayList<String> _source, AULCompilerLog _compilerLog)
    {
        if (_source == null)
        {
            throw new AULRuntimeException("Null _source argument in insertTypeChecks()");
        }

        Logger logger = AULLogging.getLogger();
        if (logger != null)
        {
            logger.debug("Inserting type checking meta data.");
        }

        ArrayList<String> source = new ArrayList<>();
        source.addAll(_source);

        ArrayList<DataTyping> constDataTypings = ConstIdiom.getDataTypings(source, true, _compilerLog);
        ArrayList<DataTyping> memDataTypings = MemIdiom.getDataTypings(source, true, _compilerLog);

        ArrayList<DataTyping> allTypings = new ArrayList<>();

        allTypings.addAll(constDataTypings);
        allTypings.addAll(memDataTypings);


        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 0));
        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 1));
        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 2));
        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 3));
        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 4));
        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 5));
        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 6));
        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 7));
        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 8));
        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 9));
        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 10));
        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 11));
        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 12));
        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 13));
        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 14));
        allTypings.add(new DataTyping(ParserEngine.LABEL_TYPE, ParserEngine.LABEL_TYPE + 15));


        ArrayList<Alias> aliases = new ArrayList<>();

        for (DataTyping typing : allTypings)
        {
            aliases.add(typing.getAlias());
        }

        return AliasPseudoIdiom.dealiasSource(source, aliases);
    }

    public ArrayList<String> partialCompile(ArrayList<String> _source, boolean _loadMode, AULCompilerLog _compilerLog)
    {
        if (_source == null)
        {
            throw new AULRuntimeException("Null _source argument in partialCompile()");
        }

        Logger logger = AULLogging.getLogger();
        if (logger != null)
        {
            logger.debug("Partial compiling.");
        }

        ArrayList<String> source = new ArrayList<>();
        source.addAll(_source);

        ArrayList<String> byteCode = new ArrayList<>();

        for (String line : source)
        {
            line = line.toLowerCase().trim();

            boolean success = false;

            for (IIdiom idiom : idioms)
            {
                ArrayList<String> compiledIdiom = idiom.compileToIdiom(line, this, _loadMode, _compilerLog);
                if (compiledIdiom != null)
                {
                    byteCode.addAll(compiledIdiom);
                    success = true;
                    break;
                }
            }

            if (!success)
            {
                if(_loadMode)
                {
                    _compilerLog.addCompilerOutput("Unable to compile line: " + line);
                    byteCode.add(line);
                } else
                {
                    return null;
                }
            }
        }

        return byteCode;
    }

    public JumpMemory buildJumpMemory(ArrayList<String> _byteCode, AULCompilerLog _compilerLog)
    {
        Logger logger = AULLogging.getLogger();
        if (logger != null)
        {
            logger.debug("Building jump memory.");
        }

        return PlaceLabelIdiom.buildJumpMemory(_byteCode, _compilerLog);
    }

    public byte[] finalCompile(ArrayList<String> _byteCode, ByteCodeEngine _engine, AULCompilerLog _compilerLog)
    {
        if (_byteCode == null || _engine == null)
        {
            throw new AULRuntimeException("Null arguments in finalCompile()");
        }

        Logger logger = AULLogging.getLogger();
        if (logger != null)
        {
            logger.debug("Finalizing compilation.");
        }

        ArrayList<String> tempByteCode = new ArrayList<>();
        tempByteCode.addAll(_byteCode);

        IOperator[] operators = _engine.getOperators();

        byte[] bytes = new byte[tempByteCode.size()];

        for (int i = 0; i < tempByteCode.size(); i++)
        {
            String opCode = tempByteCode.get(i);

            boolean success = false;

            for (int k = 0; k < ASEConstants.MAX_OPCODES; k++)
            {
                IOperator operator = operators[k];

                if (operator == null)
                {
                    continue;
                }

                if (opCode.equals(operator.getKeyword().toLowerCase()))
                {
                    bytes[i] = (byte) operator.getBaseCode();
                    success = true;
                    break;
                }
            }

            if (!success)
            {
                _compilerLog.addCompilerOutput("Unknown opcode: " + opCode);
            }
        }

        return bytes;
    }
}
