package testbed;

import amp.AmpLogging;
import aul_compiler.AULCompilerLog;
import aul_compiler.AULCompiler;
import aul_compiler.AULLogging;
import aul_compiler.parser.ParserEngine;
import engine.ASEConstants;
import engine.ASELogging;
import engine.ByteCodeEngine;
import engine.data.ConstantMemory;
import engine.data.DataElement;
import engine.data.JumpMemory;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * ASE Utility Language Compiler created by John Minor.
 */
public class AULTestbed
{
    public static void main(String[] args) throws Exception
    {

        System.setProperty("log4j.configurationFile", "aullog4j.xml");

        AmpLogging.startLogging();
        ASELogging.startLogging();
        AULLogging.startLogging();

        System.out.println();

        byte[] entropy = new byte[ASEConstants.ENTROPY_MAX_MAX_LENGTH];
        long timestamp = System.currentTimeMillis();
        byte[] publicKey = new byte[ASEConstants.PUBLIC_KEY_MAX_LENGTH];

        AULCompiler compiler = new AULCompiler();

        ByteCodeEngine engine = new ByteCodeEngine(1);
        engine.addReservedWord8Operators();
        engine.finalizeOperators();

        String sourceString =
                "def const[] = MY_INT[0~5]\n" +
                "def const[] = MY_INT[5~7]\n" +
                "def memory[] = memInt[0~5]\n" +
                "def label0 = a\n" +
                "def label1 = b\n" +
                "def label2 = c\n" +
                "\n" +
                "\n" +
                "int MY_INT[5] = 1\n" +
                "int MY_INT[6] = 2\n" +
                "int MY_INT[7] = 3\n" +
                "\n" +
                "for(i = 0~5)\n" +
                "{\n" +
                "   int memInt[i]\n" +
                "}\n" +
                "\n" +
                "\n" +
                "if(MY_INT[5] == MY_INT[6]) else go to (b)\n" +
                "place a\n" +
                "const14\n" +
                "place b\n" +
                "memory29\n" +
                "place c\n" +
                "label9";

        AULCompilerLog compilerLog = new AULCompilerLog();

        ArrayList<String> source = ParserEngine.separateLinesAndAddLineNumbers(sourceString);

        ArrayList<String> noBlanksSource = compiler.removeBlankSpace(source);

        ArrayList<String> dealiasedSource = compiler.dealiasSource(noBlanksSource);

        ConstantMemory constantMemory = compiler.buildConstantMemory(dealiasedSource, compilerLog);

        if(constantMemory == null)
        {
            return;
        }

        ArrayList<String> typeCheckedSource = compiler.insertTypeChecks(dealiasedSource, compilerLog);

        ArrayList<String> byteCode = compiler.partialCompile(typeCheckedSource, true, compilerLog);

        JumpMemory jumpMemory = compiler.buildJumpMemory(byteCode, compilerLog);

        byte[] compiledProgram = compiler.finalCompile(byteCode, engine, compilerLog);

        Logger logger = AULLogging.getLogger();

        System.out.println();

        if(logger!=null)
        {
            logger.debug("AND NOW FOR THE SOURCE:\n");
        }

        if(source != null)
        {
            for (String line : source)
            {
                if(logger!=null)
                {
                    System.out.println(line);
                    //logger.debug(line);
                }
            }
        }

        if(logger!=null)
        {
            logger.debug("AND NOW FOR THE PROCESSED SOURCE:\n");
        }

        if(typeCheckedSource != null)
        {
            for (String line : typeCheckedSource)
            {
                if(logger!=null)
                {
                    System.out.println(line);
                    //logger.debug(line);
                }
            }
        }

        if(logger!=null)
        {
            logger.debug("AND NOW FOR THE BYTE CODE:\n");
        }

        if (byteCode != null)
        {
            for (String line : byteCode)
            {
                if(logger!=null)
                {
                    System.out.println(line);
                    //logger.debug(line);
                }
            }
        }

        if(logger!=null)
        {
            logger.debug("AND NOW FOR THE PROGRAM BYTES:\n");
        }

        if(compiledProgram != null)
        {
            for(int i = 0; i < compiledProgram.length; i++)
            {
                System.out.println(compiledProgram[i]);
            }
        }

        if(logger!=null)
        {
            logger.debug("AND NOW FOR THE CONSTANT MEMORY:");
        }

        if (constantMemory != null)
        {
            for (int i = 0; i < ASEConstants.CONSTANT_MEMORY_MAX_DATA_ELEMENTS; i++)
            {
                DataElement element = constantMemory.getElement(i);
                if(logger!=null && element != null)
                {
                    System.out.println("\nElement " + i + " as a String and a Big Integer:");
                    System.out.println(element.getDataAsString());
                    System.out.println(element.getDataAsBigInteger().toString(10));
                }
            }
        }

        if(logger!=null)
        {
            logger.debug("AND NOW FOR THE JUMP MEMORY:\n");
        }

        if(jumpMemory != null)
        {
            for(int i = 0; i < ASEConstants.JUMP_MEMORY_MAX_ELEMENTS; i++)
            {
                System.out.println(jumpMemory.getJumpPoint(i));
            }
        }

        if(logger != null)
        {
            logger.debug("AND NOW FOR THE COMPILER ERRORS");
        }

        for(String err: compilerLog.getCompilerOutput())
        {
            System.out.println(err);
        }

        System.out.println();
    }
}
